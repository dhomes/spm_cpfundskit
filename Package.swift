// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let major=1
let minor=0
let point=10010

let package = Package(
    name: "CPFundsKit",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "CPFundsKit",
            targets: ["CPFundsKit"])
    ],
    dependencies: [
        .package(url: "https://bitbucket.org/dhomes/spm_cpcorekit.git", branch: "master"), // change to latest version (buscar)
        .package(url: "https://bitbucket.org/dhomes/spm_cpcloudkit.git", branch: "master"),
        .package(url: "https://bitbucket.org/dhomes/spm_cploginkit.git", branch: "master"),
        .package(url: "https://bitbucket.org/dhomes/spm_cppaykit.git", branch: "master")
    ],
    targets: [
        .binaryTarget(
            name: "CPFundsKit",
            url: "https://bitbucket.org/dhomes/spm_cpfundskit/downloads/CPFundsKit.xcframework.zip",
            checksum: "3b33eaaff75abde39dce2f105fb0536a4ece8cac01fcefef7b02188279069a7b"
        )
    ]
    
)
